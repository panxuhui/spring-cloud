/******************************************************************************
 *                         Libra FRAMEWORK
 *           © Libra framework, (2020). All rights reserved.
 *
 *  All rights are reserved. Reproduction in whole or in part is prohibited
 *  without the written consent of the copyright owner.
 *****************************************************************************/
package com.hxupan.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This controller for demo.
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

	/* public methods ------------------------------------------------------ */

	@GetMapping(path = "/hello")
	public String hello(String content) {
		return "hello, " + content;
	}
}

