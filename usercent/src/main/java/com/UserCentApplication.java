/******************************************************************************
 *                         Libra FRAMEWORK
 *           © Libra framework, (2020). All rights reserved.
 *
 *  All rights are reserved. Reproduction in whole or in part is prohibited
 *  without the written consent of the copyright owner.
 *****************************************************************************/
package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 用户中心服务.
 */
@EnableDiscoveryClient
@SpringBootApplication
public class UserCentApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserCentApplication.class, args);
	}
}

