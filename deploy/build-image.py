import os
import argparse

parser = argparse.ArgumentParser(description='argument define.')
parser.add_argument('--pull', '-p', help='是否拉取代码')

isPullCode = input("是否需要拉取代码(y/n):")
if isPullCode == 'y':
    os.system("git branch")
    os.system("git pull")
    os.system("mvn clean install -Dmaven.skip.test=true -f ../pom.xml")

# cloud-discovery
serviceName = input("请输入要构建镜像的服务: ")
# cloud-discovery-1.0.jar
jarName = serviceName + "-1.0.jar"

# copy jar to current directory
copyJarCmd = "cp ../{serviceName}/target/{jarName} ./".format(serviceName=serviceName, jarName=jarName)
os.system(copyJarCmd)
# copy lib directory to current directory
copyLibCmd = "cp -ri ../{serviceName}/target/lib/ ./".format(serviceName=serviceName)
os.system(copyLibCmd)

buildCmd = "time docker build --build-arg JAR_NAME={jarName} -t {serviceName}:1.0 .".format(jarName=jarName, serviceName=serviceName)
os.system(buildCmd)

# 删除构建后的jar, lib
removeJarCmd = "rm -r %s %s" % (jarName, "lib")
os.system(removeJarCmd)
